#include <stdio.h>      
#include <sys/types.h>
#include <sys/socket.h>   
#include <netdb.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h> 
#include <time.h>
#include <limits.h>

int parse_long(char *s, long *ret) {
    if (!ret) {
        return -1;
    }
    char *end = s;
    long valid = strtol(s, &end, 10);
    if (!end) {
        return -1;
    }
    *ret = valid;
    return 0;
}

int main(int argc, char *argv[]) {

    int simpleSocket = 0;
    int simplePort = 0;
    int returnStatus = 0;
    struct sockaddr_in simpleServer;

    if (2 != argc) {

        fprintf(stderr, "Usage: %s <port>\n", argv[0]);
        exit(1);
    }

    simpleSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

    if (simpleSocket == -1) {

        fprintf(stderr, "Could not create a socket!\n");
        exit(1);

    }
    else {
	    fprintf(stderr, "Socket created!\n");
    }

    /* retrieve the port number for listening */
    simplePort = atoi(argv[1]);

    /* setup the address structure */
    /* use INADDR_ANY to bind to all local addresses  */
    memset(&simpleServer, '\0', sizeof(simpleServer)); 
    simpleServer.sin_family = AF_INET;
    simpleServer.sin_addr.s_addr = htonl(INADDR_ANY);
    simpleServer.sin_port = htons(simplePort);

    /*  bind to the address and port with our socket  */
    returnStatus = bind(simpleSocket,(struct sockaddr *)&simpleServer,sizeof(simpleServer));

    if (returnStatus == 0) {
	    fprintf(stderr, "Bind completed!\n");
    }
    else {
        fprintf(stderr, "Could not bind to address!\n");
	close(simpleSocket);
	exit(1);
    }

    /* lets listen on the socket for connections      */
    returnStatus = listen(simpleSocket, 5);

    if (returnStatus == -1) {
        fprintf(stderr, "Cannot listen on socket!\n");
	close(simpleSocket);
        exit(1);
    }

    long max = LONG_MIN;
    while (1)
    {
        struct sockaddr_in clientName = { 0 };
        int simpleChildSocket = 0;
        int clientNameLength = sizeof(clientName);

        /* wait here */
        simpleChildSocket = accept(simpleSocket,(struct sockaddr *)&clientName, &clientNameLength);

        if (simpleChildSocket == -1) {
            fprintf(stderr, "Cannot accept connections!\n");
            close(simpleSocket);
            exit(1);
        }
        puts("connection accepted");

        char buffer[16] = { 0 };
        int returnStatus = read(simpleChildSocket, buffer, sizeof(buffer));
        if ( returnStatus > 0 ) {
            printf("%d: read %s\n", returnStatus, buffer);
        } else {
            continue;
        }
        
        long num;
        int res = parse_long(buffer, &num);
        if (res < 0) {
            // FIXME: handling error this way makes client's life harder
            puts("invalid number");
            continue;
        }
        if (num > max) {
            max = num;
        }
        
        char buf[16];
        snprintf(buf, sizeof(buf), "%ld", max);
        write(simpleChildSocket, buf, strlen(buf));

        /* handle the new connection request  */
        /* write out our message to the client */
        close(simpleChildSocket);
    }

    close(simpleSocket);
    return 0;

}

