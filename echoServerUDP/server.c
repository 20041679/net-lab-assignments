#include <stdio.h>      
#include <sys/types.h>
#include <sys/socket.h>   
#include <netdb.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h> 
#include <time.h>
#include <limits.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define ARRAY_SIZE(x) (sizeof(x) / sizeof(*x))

int main(int argc, char *argv[]) {
    if (2 != argc) {
        fprintf(stderr, "Usage: %s <port>\n", argv[0]);
        exit(1);
    }

    int simpleSocket = socket(AF_INET, SOCK_DGRAM, 0);

    if (simpleSocket == -1) {
        fprintf(stderr, "Could not create a socket!\n");
        exit(1);
    } else {
	    puts("Socket created");
    }

    int simplePort = atoi(argv[1]);

    struct sockaddr_in serverAddr = { 0 };
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    serverAddr.sin_port = htons(simplePort);

    if (bind(simpleSocket,(struct sockaddr *)&serverAddr,sizeof(serverAddr)) == 0) {
        puts("Bind complete");
    }
    else {
        fprintf(stderr, "Could not bind to address!\n");
        close(simpleSocket);
        exit(1);
    }

    while (1) {
        struct sockaddr_in clientAddr;
        socklen_t clientAddrLen = sizeof(clientAddr);

        char message[256] = { 0 };
        // ssize_t messageLength = read(simpleChildSocket, buffer, sizeof(buffer));
        ssize_t messageLength = recvfrom(
            simpleSocket,
            message,
            ARRAY_SIZE(message),
            0,
            (struct sockaddr *)&clientAddr,
            &clientAddrLen
        );
        if (messageLength < 0) {
            continue;
        }

        printf("Received %s from %s\n", message, inet_ntoa(clientAddr.sin_addr));

        sendto(
            simpleSocket,
            message,
            messageLength,
            0,
            (struct sockaddr *)&clientAddr,
            clientAddrLen
        );
    }

    close(simpleSocket);
    return 0;

}

