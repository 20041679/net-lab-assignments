#include <stdio.h>      
#include <sys/types.h>
#include <sys/socket.h>   
#include <netdb.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <string.h>

#define ARRAY_SIZE(x) (sizeof(x) / sizeof(*x))

void strip(char *s) {
    char *pos = strchr(s, '\n');
    if (!pos) {
        *pos = '\0';
    }
}

int main(int argc, char *argv[]) {
    if (3 != argc) {
        fprintf(stderr, "Usage: %s <server> <port>\n", argv[0]);
        exit(1);
    }

    int simplePort = atoi(argv[2]);

    while (1) {
        int simpleSocket = socket(AF_INET, SOCK_DGRAM, 0);

        if (simpleSocket == -1) {
            fprintf(stderr, "Could not create a socket!\n");
            exit(1);
        } else {
            puts("Socket created");
        }

        struct sockaddr_in serverAddr = { 0 };
        serverAddr.sin_family = AF_INET;
        serverAddr.sin_addr.s_addr=inet_addr(argv[1]);
        serverAddr.sin_port = htons(simplePort);
        socklen_t serverAddrLen = sizeof(serverAddr);

        puts("inserisci frase");
        char message[256];
        fgets(message, ARRAY_SIZE(message), stdin);
        message[strcspn(message, "\n")] = '\0';

        sendto(
            simpleSocket,
            message,
            strlen(message),
            0,
            (struct sockaddr *)&serverAddr,
            serverAddrLen
        );

        ssize_t messageLength = recvfrom(
            simpleSocket,
            message,
            ARRAY_SIZE(message),
            0,
            (struct sockaddr *)&serverAddr,
            &serverAddrLen
        );
        if (messageLength < 0) {
            continue;
        }
        size_t _messageLength = (size_t)messageLength;
        message[_messageLength < ARRAY_SIZE(message)
            ? _messageLength
            : ARRAY_SIZE(message) - 1] = '\0';
        puts(message);

        close(simpleSocket);
    }
    return 0;
}

