#include <stdio.h>      
#include <sys/types.h>
#include <sys/socket.h>   
#include <netdb.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>

#define ARRAY_SIZE(x) (sizeof(x) / sizeof(*x))

int expect_str(int fd, char *s) {
    char buffer[256];
    ssize_t ret = read(fd, buffer, ARRAY_SIZE(buffer));
    if (ret < 0) {
        return ret;
    }
    return memcmp(buffer, s, strlen(s)) == 0;
}

int main(int argc, char *argv[]) {
    int simpleSocket = 0;
    int simplePort = 0;
    int returnStatus = 0;
    struct sockaddr_in simpleServer;

    if (3 != argc) {
        fprintf(stderr, "Usage: %s <server> <port>\n", argv[0]);
        exit(1);
    }

    /* create a streaming socket      */
    simpleSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

    if (simpleSocket == -1) {

        fprintf(stderr, "Could not create a socket!\n");
        exit(1);

    }
    else {
        fprintf(stderr, "Socket created!\n");
    }

    /* retrieve the port number for connecting */
    simplePort = atoi(argv[2]);

    /* setup the address structure */
    /* use the IP address sent as an argument for the server address  */
    //bzero(&simpleServer, sizeof(simpleServer)); 
    memset(&simpleServer, '\0', sizeof(simpleServer));
    simpleServer.sin_family = AF_INET;
    //inet_addr(argv[2], &simpleServer.sin_addr.s_addr);
    simpleServer.sin_addr.s_addr=inet_addr(argv[1]);
    simpleServer.sin_port = htons(simplePort);

    /*  connect to the address and port with our socket  */
    returnStatus = connect(simpleSocket, (struct sockaddr *)&simpleServer, sizeof(simpleServer));

    if (returnStatus == 0) {
        fprintf(stderr, "Connect successful!\n");
    }
    else {
        fprintf(stderr, "Could not connect to address!\n");
        close(simpleSocket);
        exit(1);
    }

    puts("# iterations");
    char buffer[256];
    fgets(buffer, ARRAY_SIZE(buffer), stdin);
    write(simpleSocket, buffer, strlen(buffer));
    if (!expect_str(simpleSocket, "ACK")) {
        goto CLOSE;
    }
    puts("valid number");

    // no error checking on client
    long n_iterations = atol(buffer);
    for (long i = 0; i < n_iterations; ++i) {
        fgets(buffer, ARRAY_SIZE(buffer), stdin);
        if (write(simpleSocket, buffer, strlen(buffer)) < 0) {
            break;
        }
        ssize_t returnStatus = read(simpleSocket, buffer, sizeof(buffer));
        if (returnStatus < 0) {
            break;
        }
        if (returnStatus > 0) {
            buffer[returnStatus - 1] = '\0';
            puts(buffer);
        }
    }
    if (write(simpleSocket, "BYE", 3) < 0) {
        goto CLOSE;
    }
    puts("BYE sent");
    if (!expect_str(simpleSocket, "ACK")) {
        goto CLOSE;
    }
    puts("ACK received");

CLOSE:
    puts("closing");
    close(simpleSocket);

    return 0;
}

