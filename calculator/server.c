#include <stdio.h>      
#include <sys/types.h>
#include <sys/socket.h>   
#include <netdb.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h> 
#include <time.h>
#include <limits.h>

#define ARRAY_SIZE(x) (sizeof(x) / sizeof(*x))

int expect_str(int fd, char *s) {
    char buffer[256];
    ssize_t ret = read(fd, buffer, ARRAY_SIZE(buffer));
    if (ret < 0) {
        return ret;
    }
    return memcmp(buffer, s, strlen(s)) == 0;
}

int compute(int a, int b, char op) {
    switch (op) {
        case '+':
            return a + b;
        case '-':
            return a - b;
        case '*':
            return a * b;
        case '/':
            return a / b;
    }
    // unreachable
    return 0;
}

int main(int argc, char *argv[]) {
    int simpleSocket = 0;
    int simplePort = 0;
    int returnStatus = 0;

    if (2 != argc) {

        fprintf(stderr, "Usage: %s <port>\n", argv[0]);
        exit(1);
    }

    simpleSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

    if (simpleSocket == -1) {
        fprintf(stderr, "Could not create a socket!\n");
        exit(1);
    } else {
	    fprintf(stderr, "Socket created!\n");
    }

    /* retrieve the port number for listening */
    simplePort = atoi(argv[1]);

    struct sockaddr_in simpleServer = { 0 };
    /* setup the address structure */
    /* use INADDR_ANY to bind to all local addresses  */
    simpleServer.sin_family = AF_INET;
    simpleServer.sin_addr.s_addr = htonl(INADDR_ANY);
    simpleServer.sin_port = htons(simplePort);

    /*  bind to the address and port with our socket  */
    returnStatus = bind(simpleSocket,(struct sockaddr *)&simpleServer,sizeof(simpleServer));

    if (returnStatus == 0) {
	    fprintf(stderr, "Bind completed!\n");
    }
    else {
        fprintf(stderr, "Could not bind to address!\n");
        close(simpleSocket);
        exit(1);
    }

    /* lets listen on the socket for connections      */
    returnStatus = listen(simpleSocket, 5);

    if (returnStatus == -1) {
        fprintf(stderr, "Cannot listen on socket!\n");
        close(simpleSocket);
        exit(1);
    }

    while (1) {
        struct sockaddr_in clientName = { 0 };
        int simpleChildSocket = 0;
        socklen_t clientNameLength = sizeof(clientName);

        /* wait here */
        simpleChildSocket = accept(simpleSocket,(struct sockaddr *)&clientName, &clientNameLength);

        if (simpleChildSocket == -1) {
            fprintf(stderr, "Cannot accept connections!\n");
            close(simpleSocket);
            exit(1);
        }
        puts("connection accepted");

        char buffer[64] = { '\0' };
        ssize_t returnStatus = read(simpleChildSocket, buffer, sizeof(buffer));
        if (returnStatus <= 0) {
            goto CLOSE;
        }
        printf("read %s\n", buffer);

        int a;
        int b;
        char op;
        int elementsRead = sscanf(buffer, "%d %d %c", &a, &b, &op);
        if (elementsRead != 3 || (op != '+' && op != '-' && op != '*' && op != '/') || (op == '/' && b == 0)) {
            puts("invalid format detected");
            write(simpleChildSocket, "NACK", 4);
            goto CLOSE;
        }
        write(simpleChildSocket, "ACK", 3);

        int res = compute(a, b, op);
        int resLen = snprintf(buffer, ARRAY_SIZE(buffer), "%d", res);
        if (resLen < 0) {
            goto CLOSE;
        }

        printf("sending result %s with len %d\n", buffer, resLen);
        write(simpleChildSocket, buffer, resLen);
CLOSE:
        close(simpleChildSocket);
    }

    close(simpleSocket);
    return 0;

}

