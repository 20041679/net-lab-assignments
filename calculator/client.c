#include <stdio.h>      
#include <sys/types.h>
#include <sys/socket.h>   
#include <netdb.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/time.h>

#define ARRAY_SIZE(x) (sizeof(x) / sizeof(*x))

int expect_str(int fd, char *s) {
    char buffer[256];
    ssize_t ret = read(fd, buffer, ARRAY_SIZE(buffer));
    if (ret < 0) {
        return ret;
    }
    return memcmp(buffer, s, strlen(s)) == 0;
}

int main(int argc, char *argv[]) {
    int simpleSocket = 0;
    int simplePort = 0;
    int returnStatus = 0;
    struct sockaddr_in simpleServer;

    if (3 != argc) {
        fprintf(stderr, "Usage: %s <server> <port>\n", argv[0]);
        exit(1);
    }

    /* create a streaming socket      */
    simpleSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

    if (simpleSocket == -1) {
        fprintf(stderr, "Could not create a socket!\n");
        exit(1);
    } else {
        puts("Socket created!");
    }

    /* retrieve the port number for connecting */
    simplePort = atoi(argv[2]);

    /* setup the address structure */
    /* use the IP address sent as an argument for the server address  */
    //bzero(&simpleServer, sizeof(simpleServer)); 
    memset(&simpleServer, '\0', sizeof(simpleServer));
    simpleServer.sin_family = AF_INET;
    //inet_addr(argv[2], &simpleServer.sin_addr.s_addr);
    simpleServer.sin_addr.s_addr=inet_addr(argv[1]);
    simpleServer.sin_port = htons(simplePort);

    /*  connect to the address and port with our socket  */
    returnStatus = connect(simpleSocket, (struct sockaddr *)&simpleServer, sizeof(simpleServer));

    if (returnStatus == 0) {
        puts("Connect successful!");
    } else {
        fprintf(stderr, "Could not connect to address!\n");
        close(simpleSocket);
        exit(1);
    }

    puts("expression");
    int a, b;
    char op;
    scanf("%d %d %c", &a, &b, &op);
    char buffer[64];
    int len = snprintf(buffer, ARRAY_SIZE(buffer), "%d %d %c", a, b, op);

    write(simpleSocket, buffer, len);

    struct timeval startTime;
    gettimeofday(&startTime,NULL);

    // server-side error checking
    if (!expect_str(simpleSocket, "ACK")) {
        fprintf(stderr, "server rejection\n");
        goto CLOSE;
    }
    puts("ACK received");

    memset(buffer, '\0', ARRAY_SIZE(buffer));
    len = read(simpleSocket, buffer, ARRAY_SIZE(buffer));
    buffer[ARRAY_SIZE(buffer) - 1] = '\0';
    printf("result: %s\n", buffer);

    struct timeval endTime;
    gettimeofday(&endTime,NULL);

    double timeServ=(endTime.tv_sec+(endTime.tv_usec/1000000.0)) - (startTime.tv_sec + (startTime.tv_usec/1000000.0));
    printf("timeServ: %g\n", timeServ);

CLOSE:
    puts("closing");
    close(simpleSocket);

    return 0;
}

